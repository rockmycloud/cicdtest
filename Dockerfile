FROM python:3.9.6-buster

COPY ./src /app
WORKDIR /app

RUN pip3 install -r requirements.txt

CMD ["python", "-m", "flask", "run", "--host=0.0.0.0"]

EXPOSE 5000